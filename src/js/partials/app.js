$(window).on('load', function() {
	setTimeout(() => {
		$('body').removeClass('is-preload');
	}, 1000);
});

$('document').ready(function() {
	init();

	if ($('.sec-main').length) {
		typewriteInit();
	}
});

$(window).resize(function () {
	init();
});

function init() {

	const $window = $(window);
	const $body = $('body');
	const $niceSelect = $('.nice-select');
	const $select = $('select.form__control');
	const $header = $('.header');
	const $btnAgree = $('#btn-agree');
	const $hamburger = $('.js-hamburger');
	const $secMain = $('.sec-main');
	const $nav = $('.nav');
	const $navLink = $nav.find('.nav__link');
	const $navLinkMob = $('.mob-menu .nav .nav__link');
	const $screenSlider = $('#screen-slider');

	if ($screenSlider.length && $window.width() > 991) {

		$screenSlider.on('init', function(slick){
			/*START AOS JS*/
			AOS.init({
				easing: 'ease-out-back',
				duration: 1000,
				disable: 'mobile',
				once: true
			});
			/*END AOS JS*/
		});

		$screenSlider.slick({
			slidesToShow: 1,
			infinite: false,
			focusOnSelect: false,
			arrows: false,
			fade: true,
			cssEase: 'linear',
			dots: false,
			autoplay: false,
			speed: 500,
			nextArrow: `<button type="button" class="slick-next">Next</button>`,
			prevArrow: `<button type="button" class="slick-prev">Prev</button>`
		});

		progressCalc($screenSlider.find('.slick-current').index());

		$screenSlider.on('afterChange', function(slick, currentSlide){
			const $this = $(this);
			let index = $this.find('.slick-slide').eq(currentSlide.currentSlide).index();

			if (index === 4) {
				$body.addClass('invert');
			} else {
				$body.removeClass('invert');
			}

			if (index === 1 || index === 2 || index === 3) {
				$body.addClass('invert-logo');
			} else {
				$body.removeClass('invert-logo');
			}

			$navLink.removeClass('active');
			$navLink.filter($(`[data-index="${index + 1}"]`)).addClass('active');

			progressCalc(index);

			if ($window.width() > 991) {
				// typewriteSingleInit();

				$this.find('.slick-slide').eq(currentSlide.currentSlide).find('.typewrite-single').typewrite({
					'delay': 150, //time in ms between each letter
					'extra_char': '|', //"cursor" character to append after each display
					'trim': true, // Trim the string to type (Default: false, does not trim)
					'callback': function () {
						let getText = $(this)[0].selector[0].innerText;
						let sliceText = getText.slice(0, -1);

						$(this)[0].selector[0].innerText = sliceText;
					} // if exists, called after all effects have finished
				})
			}

			$this.find('.slick-slide').eq(currentSlide.currentSlide).find('[data-aos]').each(function (index, elem) {
				$(elem).addClass('aos-init aos-animate')
			});

		});

		$screenSlider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
			$('[data-aos]').removeClass('aos-init aos-animate');
		});


		$navLink.on('click', function (e) {
			e.preventDefault();

			const $this = $(this);
			let index = $this.data('index');

			if (!$this.hasClass('active')) {
				$navLink.removeClass('active');
				$this.addClass('active');
				$screenSlider.slick('slickGoTo', index - 1);

				if ($body.hasClass('menu-open') && $hamburger.hasClass('is-active')) {
					$hamburger.removeClass('is-active');
					$body.removeClass('menu-open');
				}
			}

		});

	}

	if ($secMain.length && $window.width() < 992) {
		$navLinkMob.on('click', function (e) {
			e.preventDefault();

			const anchor = $(this);
			let ancAtt = $(anchor.data('href'));

			$('html, body').stop().animate({
				scrollTop: ancAtt.offset().top - $header.outerHeight()
			}, 1000);

			$hamburger.removeClass('is-active');
			$body.removeClass('menu-open');
		});

		AOS.init({
			easing: 'ease-out-back',
			duration: 1000,
			once: true
		});
	}


	/*START AOS JS*/
	if (!$secMain.length) {
		AOS.init({
			easing: 'ease-out-back',
			duration: 1000,
			once: true
		});
	}
	/*END AOS JS*/

	if ($btnAgree.length) {

		$btnAgree.on('click', function () {
			$(this).closest('.popup-message').fadeOut();
		})

	}

	if ($select.length) {


		$select.niceSelect();

		$(document).on('click', function () {

			$('.nice-select').each(function (index, select) {
				if (!$(select).hasClass("open") && $(select).find('.current').text() !== 'Choose...') {
					$(select).addClass('is-dirty');
				}
			})
		})
	}

	if ($hamburger.length) {
		$hamburger.on('click', function () {
			$(this).toggleClass('is-active');
			$body.toggleClass('menu-open');
		})
	}

	$(window).scroll(function (e) {

		if ($(document).scrollTop() > 10) {
			$header.addClass('sticky')
		} else {
			$header.removeClass('sticky')
		}

	});

	function progressCalc(index) {
		const $progressParent = $('.progress');
		const $progressBar = $progressParent.find('.progress__bar');

		let widthFull = $progressParent.width();
		let allStep = $('.sec').length;
		let step = widthFull / allStep;
		let current = index + 1 || 0;

		$progressBar.css('width', step * current)
	}
}
