const TxtTypeSingle = function(el, toRotate, period) {
    this.toRotate = toRotate;
    this.el = el;
    this.loopNum = 0;
    this.period = parseInt(period, 10) || 2000;
    this.txt = '';
    this.tick();
    this.isDeleting = false;
};

TxtTypeSingle.prototype.tick = function() {
    let i = this.loopNum % this.toRotate.length;
    let fullTxt = this.toRotate[i];

    if (this.isDeleting) {
        this.txt = fullTxt.substring(0, this.txt.length - 1);
    } else {
        this.txt = fullTxt.substring(0, this.txt.length + 1);
    }

    this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

    const that = this;
    let delta = 200 - Math.random() * 100;

    if (this.isDeleting) { delta /= 2; }

    if (!this.isDeleting && this.txt === fullTxt) {
        delta = this.period;
        this.isDeleting = true;
    } else if (this.isDeleting && this.txt === '') {
        this.isDeleting = false;
        this.loopNum++;
        delta = 500;
    }

    setTimeout(function() {
        that.tick();
    }, delta);
};

function typewriteSingleInit() {
    const elementsSingle = document.getElementsByClassName('typewrite-single');
    let colorSingle = '#1DAF83';
    for (let i=0; i<elementsSingle.length; i++) {
        const toRotateSingle = elementsSingle[i].getAttribute('data-type');
        const periodSingle = elementsSingle[i].getAttribute('data-periodSingle');
        if (toRotateSingle) {
            new TxtTypeSingle(elementsSingle[i], JSON.parse(toRotateSingle), periodSingle);
        }
    }
    // INJECT CSS
    let cssSingle = document.createElement("style");
    cssSingle.type = "text/css";
    cssSingle.innerHTML = `.typewrite-single > .wrap { border-right: 0.08em solid ${colorSingle}`;
    document.body.appendChild(cssSingle);
}
